#ifndef B2StepAction_h
#define B2StepAction_h 1

#include "G4UserSteppingAction.hh"

#include "G4Event.hh"
#include "G4EventManager.hh"
#include "G4ios.hh"
#include "globals.hh"
#include "G4RunManager.hh"

class G4Step;
class B2EventAction;
class B2StepAction: public G4UserSteppingAction
{
public:
  B2StepAction(B2EventAction*);
 ~B2StepAction();

void UserSteppingAction(const G4Step* aStep);
  
private:
 

  G4TrackVector* fSecondary;
  B2EventAction* feventAction;
  G4double tracklength;
  G4double muonEnergy;
  G4double muonEnergyFinal;
  G4int eventID;
  G4double muonpx;
  G4double muonpy;
  G4double muonpz;
  
  std::vector<G4double> fx;
  std::vector<G4double> fy;
  std::vector<G4double> fz;
  
  
  
  std::vector<G4double> fenergy;
 std::vector<G4double> ffrad;
  
  std::vector<G4double> fWeights;
  std::vector<G4int> fPDGCode;
  std::vector<G4double> fpx;
  std::vector<G4double> fpy;
  std::vector<G4double> fpz;
 
};
#endif
