#ifndef B2StackingAction_h
#define B2StackingAction_h 1
 
#include "G4UserStackingAction.hh"
#include "globals.hh"
 


 
 class B2StackingAction : public G4UserStackingAction
 {
   public:
     B2StackingAction();
     virtual ~B2StackingAction();
      
     virtual G4ClassificationOfNewTrack ClassifyNewTrack(const G4Track*);        
 };
 
 //....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

 #endif
