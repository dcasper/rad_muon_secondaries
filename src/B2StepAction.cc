#include "B2StepAction.hh"
#include "G4SteppingManager.hh"
#include "G4Step.hh"
#include "B2RunAction.hh"
#include "B2EventAction.hh"
#include "G4Run.hh"
#include "B2AnalysisManager.hh"
#include "G4RunManager.hh"
#include "G4ParticleDefinition.hh"
#include "TTree.h"
#include "G4UImanager.hh"
#include "G4SystemOfUnits.hh"
#include "G4UnitsTable.hh"
//#include "B2Run.hh"
#include "G4Event.hh"
#include<cmath>



B2StepAction::B2StepAction(B2EventAction* eventAction)
  :G4UserSteppingAction(),feventAction(eventAction),tracklength(0.),muonEnergy(0.),muonEnergyFinal(0.),muonpx(0.),muonpy(0.),muonpz(0.),fenergy(0.),ffrad(0.),fPDGCode(0.),fpx(0.),fpy(0.),fpz(0.),fx(0.),fy(0.),fz(0.)
{

}

B2StepAction::~B2StepAction()
{
}


void B2StepAction::UserSteppingAction(const G4Step* aStep)
{
  //reset variables (besides muon track length) for each step

  fenergy.clear();
  ffrad.clear();
  fPDGCode.clear();
  fpx.clear();
  fpy.clear();
  fpz.clear();
 fx.clear();
  fy.clear();
  fz.clear();
  
  //set flag to false-flag to tell if secondar>emin was produced
  bool flag = false;

  //minimum secondary energy to be recorded
  static G4double eMin= 30 *GeV;
  //GeV


  //G4SteppingManager* steppingManager = fpSteppingManager;

 G4Track* theTrack = aStep->GetTrack();

 

 
 //if(theTrack->GetTrackStatus()==fAlive){return;}



if(theTrack->GetParentID()==0)
  {
    muonEnergyFinal = (theTrack->GetKineticEnergy())/1000;
   muonpx = (theTrack->GetMomentumDirection())[0];
 muonpy = (theTrack->GetMomentumDirection())[1];
 muonpz = (theTrack->GetMomentumDirection())[2];

    tracklength =(theTrack->GetTrackLength())/1000;
    muonEnergy =(theTrack->GetDynamicParticle()->GetPrimaryParticle()->GetTotalEnergy())/1000;
    
  }

const G4Event* event = G4RunManager::GetRunManager()->GetCurrentEvent();
      
      // Get the event ID
 G4int eventID =(event->GetEventID());

 

 
// tracklength = sqrt(pow(pos[0],2)+pow(pos[1],2)+pow((pos[2]+60),2));


//muon track length

//tracklength =  feventAction->RetrieveTrackLength();
  
 //retrieve secondary particle info
 // G4TrackVector* fsecondary = steppingManager-> GetfSecondary();
 auto fsecondary = aStep->GetSecondaryInCurrentStep();
 
  if ((*fsecondary).size()>0)
  {
   

 //std::cout<<" "<< muonEnergy;
 //loop through each secondary
  for (size_t lp1 =0; lp1<(*fsecondary).size();lp1++)
    {
      
      //determine if secondary is has min kinetic energy
      if (((*fsecondary)[lp1]->GetKineticEnergy())>=eMin)
	{

        
  

	  //retrieve variables for  secondaries
 G4int code = (*fsecondary)[lp1]->GetParticleDefinition()->GetPDGEncoding();

 G4double energy = ((*fsecondary)[lp1]->GetKineticEnergy())/1000;
 
 G4double frad = energy/muonEnergy;
 G4double secondaryDirectionX = (*fsecondary)[lp1]->GetMomentumDirection()[0];
 G4double secondaryDirectionY = (*fsecondary)[lp1]->GetMomentumDirection()[1];
 G4double secondaryDirectionZ = (*fsecondary)[lp1]->GetMomentumDirection()[2];
 G4double secondaryPosx = ((*fsecondary)[lp1]->GetPosition()[0])/100;
 G4double secondaryPosy = ((*fsecondary)[lp1]->GetPosition()[1])/100;
 G4double secondaryPosz = ((*fsecondary)[lp1]->GetPosition()[2])/1000 +60;

 //add variable to respective vectors for each secondary
  fPDGCode.push_back(code);
  fenergy.push_back(energy);
  ffrad.push_back(frad);
  fpx.push_back(secondaryDirectionX);
  fpy.push_back(secondaryDirectionY);
  fpz.push_back(secondaryDirectionZ);

 fx.push_back(secondaryPosx);
  fy.push_back(secondaryPosy);
  fz.push_back(secondaryPosz);
     
      //make flag true to signal the tree to be filled
  flag = true;   

	}
    }
  // G4int pid = theTrack->GetParentID();

  //secondary with energy greater than emin created, so fill the tree and abort event to move on to next muon
 
  





      if (flag == true)
	{
	 

	  B2AnalysisManager::getInstance()->AddEvent(fenergy,ffrad,fPDGCode,fpx,fpy,fpz,tracklength,muonEnergyFinal,muonpx,muonpy,muonpz,eventID,fx,fy,fz);
        
  G4UImanager* UImanager = G4UImanager::GetUIpointer();
  UImanager->ApplyCommand("/event/abort");
  // G4cout <<"   " <<fx[0] << "  "<<G4BestUnit(fx[0],"Length") <<"  " <<tracklength<<G4endl;
	  

	}
	
      }




}


